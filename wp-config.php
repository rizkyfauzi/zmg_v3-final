<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'zmg_v3');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/** Set up URL */
//define('WP_HOME', 'http://dwisign.com/project/zmgrevamp/wp');
//define('WP_SITEURL', 'http://dwisign.com/project/zmgrevamp/wp');
/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '!Cbdh7+b<(YtF)JOp;;H6~8!.o[PXq!uhf7(2Ws1q_T;X>uG|i$aRfX0ikJS0KnA');
define('SECURE_AUTH_KEY',  '`-<3UEa[WYK&Ejx81OWdzPvD@kk7_DyTIx L5Ye)95Na(DF[fx}g5z~-87pvuv0;');
define('LOGGED_IN_KEY',    'd,Z6GI`z7U?`n][Jp/=S0*4D;BpT#,+b$+V|8Hc3b^2)xkQ`EPAMR;P{i}{[6IRC');
define('NONCE_KEY',        'I:>cM%sZc!5h@2AHB|f;G`dPPL[G0&>*@A26~`wT%zS?g*d-*Vb4wYg}90jNrn+?');
define('AUTH_SALT',        'pt=(mgJ3k_6KfSI$d3)~&70~s$&A[L)8}4Md1:ItNi{S|0/iXW|[JRIdvcDd,pIC');
define('SECURE_AUTH_SALT', 'jBEfeNfm7~EOAaz)&H<_p0^pYrHbzGZ7j%,hJ`kw7>xz#ICA(5_/q#9>Uf4&=~``');
define('LOGGED_IN_SALT',   'T3A0>U]zM)bY1,e1DI/f@m4e%vi?n0WDmEDq>]&CTP=aC<!cViF*{_/$L=T)Vxma');
define('NONCE_SALT',       '=p<$m<,9cSo{r~|B(sDp;lQ:~S16<pX5,.Sz,7qxoA>/Z<bZ`6KRWR}PoqDHG(Xg');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wpZMGV3_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
