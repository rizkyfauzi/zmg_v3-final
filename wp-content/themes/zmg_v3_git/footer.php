	<div class="footer">
	  <div class="container">
	  	<div class="row">
	      <div class="col-md-3 col-sm-6 logo-footer">
	        </br><img src="<?php bloginfo('template_directory'); ?>/image/footer_logo.png" width="120">
	      </div>
	  		
	      <div class="col-md-3 col-sm-6">
	        <!-- <h5 class="title">MENU</h5>
	        <ul class="link">
	          <li><a href="#">About Us</a></li>
	          <li><a href="#">Our Services</a></li>
	          <li><a href="#">Career</a></li>
	          <li><a href="#">Contact</a></li>
	        </ul> -->
	        <?php if ( is_active_sidebar( 'footer_2' ) ) : ?> 
			    <?php dynamic_sidebar( 'footer_2' ); ?>
			<?php endif; ?>
	      </div>
	  	  <div class="col-md-3 col-sm-6">
  			<!-- <h5 class="title">INFORMATION</h5>
  			<ul class="link">
  				<li><a href="#">Network & Planning Design</a></li>
  				<li><a href="#">Civil Work</a></li>
  				<li><a href="#">Equipment Installation</a></li>
  				<li><a href="#">Radio Network Optimization</a></li>
  				<li><a href="#">Maintenance Service</a></li>
  			</ul> -->
  			<?php if ( is_active_sidebar( 'footer_3' ) ) : ?> 
			    <?php dynamic_sidebar( 'footer_3' ); ?>
			<?php endif; ?>
	  	  </div>
	      <div class="col-md-3 col-sm-6">
	        <!-- <h5 class="title">ADDRESS</h5>
	        <p>
	          Bellagio Mall UG Floor unit 17-18</br>
	          Jl. Mega Kuningan Barat Kav E3.4,</br> 
	          Setiabudi, South Jakarta
	        </p> -->
	        <?php if ( is_active_sidebar( 'footer_4' ) ) : ?> 
			    <?php dynamic_sidebar( 'footer_4' ); ?>
			<?php endif; ?>
	      </div>
	  	</div>   
	  </div>
	</div>

	<div class="footer-bottom">
	  <div class="container">
	    <p>Copyright <?php echo date( 'Y' ) ?> | ZMG Indonesia</p>
	  </div>
	</div>

	<?php wp_footer(); ?>
	</body>
</html>