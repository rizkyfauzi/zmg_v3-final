<?php 
/**
 * The template for displaying 404 pages (Not Found)
 *
 * @package WordPress
 * @subpackage zmgv3
 * @since zmgv3
 */

get_header(); ?>

<!--CONTENT ONE COLUMN-->
<div class="content-full-inside">
  <div class="container">
    <div class="row">
      <!-- LEFT SIDE -->
      <div class="col-sm-12">
        <div class="content-wrap">
          <div class="left-side">
            <h1></h1>
            
            <div class="jumbotron">
              <h1>OOPSS!!</h1>
              <p>We are sorry but the page you requested cannot be found</p> 
              <p><a href="http://zmg.co.id/" class="btn btn-primary btn-lg">Back to Homepage</a></p>
            </div>
            
          </div>
        </div>
      </div>
      <!-- END LEFT SIDE -->
    </div>
  </div><!--END CONTAINER-->
</div> 
<!--END CONTENT TWO COLUMN-->

<?php get_footer(); ?>